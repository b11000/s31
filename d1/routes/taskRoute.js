const express = require('express');
const taskController = require('../controllers/taskController');
const router = express.Router();

// Route to get all the tasks
// http://localhost:3001/tasks/
router.get("/",(req,res) =>{
	taskController.getAllTasks().then(resultFromController=>res.send(resultFromController));
})

/*==>==>==>==>Activity<==<==<==<==*/
// 1. Create a route for getting a specific task.
router.get("/:id",(req, res)=>{
	taskController.getOneTask(req.params.id).then(result=>res.send(result));
})


// Create a route for creating a task
router.post("/",(req, res)=>{
	taskController.createTask(req.body).then(result=>res.send(result));
})
// Route for deleting a task.
// http://localhost:3001/tasks/:id
// The colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
// The word that coms after the colon symbol will be the name of the URL parameter
// ":id" is called WILDCARD where you can put any value, it then creates a link between "id" parameter in the url and the value provided in the URL
router.delete("/:id", (req, res)=>{
	// URL parameter values are accessed via the request object's "params" property
	// The property name of this object will match the given URL parameter name
	// In this case "id" is the name of the parameter
	taskController.deleteTask(req.params.id).then(result=>res.send(result))
})//wildcard, The ":" is an identifier

// Update a task
router.put("/:id",(req,res)=>{
	taskController.updateTask(req.params.id, req.body).then(result=>res.send(result))
})

/*==>==>==>==>Activity<==<==<==<==*/
router.put("/:patch",(req,res)=>{
	taskController.updateTask(req.params.id, req.body).then(result=>res.send(result))
})

module.exports = router;
