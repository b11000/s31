// Controllers contain the functionas and business logic of our express js app
// meaning all the operations can do will be placed in this file
const Task = require('../models/task');

// Controller function for getting all the tasks
module.exports.getAllTasks=()=>{
	return Task.find({}).then(result=>{
		return result;
	})//from task routes, returns from task routes
}
/* ==>==>==>==> Activity <==<==<==<==*/
// 2. Create a controller function for retrieving a specific task.
module.exports.getOneTask=(taskId)=>{
	return Task.findById({taskId}).then(result=>{
		return result;
	})
}


// Controlller function for creating a task
module.exports.createTask=(requestBody)=>{
	// Create a task object based on the Mongoose model "Task"
	let newTask = new Task({
		name: requestBody.name
	})
	// Save
	// The "then" method will accept the 2 parameters
		//the first parameter will store the result returned by the mongoose "save" method 
		//The second parameter will store the "error object"
	return newTask.save().then((task, error)=>{
		if(error){
			console.log(error)
			// If an error is encountered, the "return" statement will prevent any other line or code below and within the same codeblock from executing.
			// Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
			// the else statement will no longer be evaluated
			return false;
		}else{
			// if save is successful return the new task object
			return task;
		}
	})
}

// Deleting a task
/*Business Logic
1.Look for the task with the corresponding id provided in the URL/route
2.Delete the task

 */
module.exports.deleteTask=(taskId)=>{
	// the "findByIdAndRemove" Mongoose method will loof kor a task with the same id provided from the URL and remove/delete the document from MongoDB. it looks for the document using the "id" field
	return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{
		if(err){
			console.log(err);
			return false;
		}else{
			return removedTask;
		}
	})
}
// Updating a task
/*Business Logic
1. Get the task with the ID
2. Replace the task's name returned from the database with the name property from  the request body
3. Save the task
 */
module.exports.updateTask=(taskId,newContent)=>{
	return Task.findById(taskId).then((result, error)=>{
		// If an error is encountered, return a falase
		if(error){
			console.log(error);
			return false;
		}
		// Results of the "findById" method will be stored in the "result"
		// It's name property will be reassigned the value of the "name" received from the request
		result.name=newContent.name;

		return result.save().then((updatedTask, error)=>{
			if(error){
				console.log(error);
				return false
			}else{
				return updatedTask;
			}
		})
	})
}

/* ==>==>==>==> Activity <==<==<==<==*/
// 6. Create a controller function for changing the status of a task to "complete".
module.exports.updateStatus=(taskId, newStat)=>{
  return Task.findById(taskId).then((result, error)=>{
    if(error){
      console.log(error);
      return false;
    }else{
      result.status=newStat.status;
      return result.save().then((updatedStatus, error)=>{
        if(error){
          console.log(error);
          return false;
        }else{
          return updatedStatus;
        }
      });
    }
  });
};
